package com.example.demo.repository;

import com.example.demo.entity.UserEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<UserEntity, Long> {

    UserEntity findByMail(String mail);

    @Query("select c.id from UserEntity c where c.mail = ?1")
    Long checkEmail(String mail);

}
