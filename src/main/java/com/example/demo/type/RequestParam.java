package com.example.demo.type;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class RequestParam {

    private String name;
    private String surname;
    private String password;
    private String mail;
    private Long birth;


    public RequestParam(String name, String surname, String password, String mail, Long birth) {
        this.name = name;
        this.surname = surname;
        this.password = password;
        this.mail = mail;
        this.birth = birth;
    }

    public RequestParam() {
    }
}
