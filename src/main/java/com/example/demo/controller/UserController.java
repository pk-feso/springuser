package com.example.demo.controller;

import com.example.demo.entity.UserEntity;
import com.example.demo.service.UserService;
import com.example.demo.type.RequestParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.Timestamp;

@Controller
@RequestMapping("/user")
public class UserController {

    private final UserService service;

    @Autowired
    public UserController(UserService service) {
        this.service = service;
    }

    @RequestMapping(params = "action=add", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> addUser(@RequestBody RequestParam param) {
        if (param == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        try {
            service.addUser(param.getName(), param.getSurname(), param.getPassword(), param.getMail(), new Timestamp(param.getBirth() * 1000));
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity<>("Added", HttpStatus.OK);
    }

    @RequestMapping(params = "action=delete", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> removeUser(Long id) {
        if (id == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        try {
            service.removeUser(id);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }

        return new ResponseEntity<>("Deleted ", HttpStatus.OK);
    }


    @RequestMapping(params = "action=findByMail", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Object> listUser(String mail) {
        if (mail == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        try {
            UserEntity entity = service.findByMail(mail);
            if (entity != null) {
                return new ResponseEntity<>( entity, HttpStatus.OK);
            } else {
                return new ResponseEntity<>("Not found ", HttpStatus.OK);

            }
        } catch (Exception e) {
            return new ResponseEntity<>("Error during found ", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


}
