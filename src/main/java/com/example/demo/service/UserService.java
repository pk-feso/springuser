package com.example.demo.service;

import com.example.demo.entity.UserEntity;

import java.sql.Timestamp;

public interface UserService {

    void addUser(String name, String surname, String password, String email, Timestamp birth);

    void removeUser(Long id);

    UserEntity findByMail(String mail);

}
