package com.example.demo.service;

import com.example.demo.entity.UserEntity;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.LocalDate;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository repository;

    @Autowired
    public UserServiceImpl(UserRepository repository) {
        this.repository = repository;
    }

    @Override
    public void addUser(String name, String surname, String password, String email, Timestamp birth) {
        LocalDate localDate1 = birth.toLocalDateTime().toLocalDate();
        Long id = repository.checkEmail(email);
        if(id == null) {
            UserEntity entity = new UserEntity(name, surname, email, localDate1, String.valueOf(password.hashCode()));
            repository.save(entity);
        } else {
            throw new RuntimeException("User created");
        }
    }

    @Override
    public void removeUser(Long id) {
        repository.delete(id);
    }

    @Override
    public UserEntity findByMail(String mail) {
        return repository.findByMail(mail);
    }
}
