package com.example.demo.entity;

import lombok.Getter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
@Getter
@ToString
public class UserEntity {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String surname;
    private String mail;
    private LocalDate birth;
    private String password;

    public UserEntity(String name, String surname, String mail, LocalDate birth, String password) {
        this.name = name;
        this.surname = surname;
        this.mail = mail;
        this.birth = birth;
        this.password = password;
    }

    public UserEntity() {
    }
}
